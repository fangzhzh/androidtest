package com.plexure.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * @author fangzhzh
 * @since Dec 13, 18 9:58 PM
 */
interface HttpApiService {
    @POST("post")
    fun getBinResponse(@Body body: HashMap<String, String>): Single<Response<BinResponse>>
}
