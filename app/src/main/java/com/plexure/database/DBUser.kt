package com.plexure.database

import androidx.room.*

/**
 * @author fangzhzh
 * @since Dec 13, 18 10:55 PM
 */

@Dao
interface UserDao {
    @get:Query("SELECT * FROM user")
    val all: List<DBUser>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(orgUsers: List<DBUser>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(orgUsers: List<DBUser>)

    @Delete
    fun delete(orgUser: DBUser)


    @Query("DELETE FROM user")
    fun removeAll()
}

@Entity(tableName = "user")
class DBUser (
        @PrimaryKey
        var id: String = "",
        var name: String = ""
)
