package com.plexure.di;

import android.app.Application;
import android.content.res.Resources;

import com.google.gson.Gson;
import com.plexure.api.HttpApiService;
import com.plexure.application.PlexureApplication;
import com.plexure.database.PlexureDatabase;
import com.plexure.myapplication.BuildConfig;

import javax.inject.Singleton;

import androidx.room.Room;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author fangzhzh
 * @since Dec 13, 18 9:45 PM
 */
@Module
public class AppModule {
    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }
    @Provides
    @Singleton
    public Application provideApplication() {
        return  application;
    }

    @Provides
    @Singleton
    public Resources provideResources() {
        return application.getResources();
    }

    @Provides
    public Retrofit provideRetrofit() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addNetworkInterceptor(logging);
        }
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://httpbin.org/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(builder.build())
                .build();
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    public HttpApiService provideHttpApiService() {
        return provideRetrofit().create(HttpApiService.class);
    }

    @Provides
    @Singleton
    public PlexureDatabase providePlexureDatabase() {
        return Room.databaseBuilder(application.getApplicationContext(), PlexureDatabase.class, "plexure"+BuildConfig.BUILD_TYPE).build();
    }


}