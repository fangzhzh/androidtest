package com.plexure.data

import com.plexure.api.BinResponse
import com.plexure.api.HttpApiService
import com.plexure.data.store.User
import com.plexure.data.store.UserStore
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author fangzhzh
 * @since Dec 13, 18 10:24 PM
 */
@Singleton
class DataRepository @Inject constructor(private val httpApiService: HttpApiService,
                                         private val userStore: UserStore) {
    fun getApiData(): Single<BinResponse> {
        return httpApiService.getBinResponse(
                hashMapOf(
                        Pair("foo", "bar"),
                        Pair("android", "kotlin")
                )
        )
                .map {
                    if (it.isSuccessful) {
                        it.body()
                    } else {
                        BinResponse()
                    }
                }


    }

    fun getUserData(): Single<List<User>> {
        return Single.fromCallable {
            userStore.mockData()
            userStore.getAllUsers()
        }

    }
}