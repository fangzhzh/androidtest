package com.plexure.application;

import android.app.Application;
import android.content.Context;

import com.plexure.di.AppComponent;
import com.plexure.di.AppModule;
import com.plexure.di.DaggerAppComponent;

import org.androidannotations.annotations.EApplication;

import io.reactivex.disposables.Disposable;

/**
 * @author fangzhzh
 * @since Dec 13, 18 9:36 PM
 */
public class PlexureApplication extends Application {
    private AppComponent appComponent;
    private AppModule appModule;
    private static Application mInstance;
    private Disposable disposable;

    public static Context getApplication() {
        return mInstance;
    }


    public static PlexureApplication get() {
        return (PlexureApplication) mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initAppComponent();
    }

    private void initAppComponent() {
        appModule = new AppModule(this);
        appComponent = DaggerAppComponent.builder()
                .appModule(appModule)
                .build();

    }

    public AppModule getAppModule() {
        return appModule;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public void setAppComponent(AppComponent appComponent) {
        this.appComponent = appComponent;
    }


}
