package com.plexure.myapplication

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.plexure.application.PlexureApplication
import com.plexure.data.DataRepository
import com.plexure.data.store.UserStore
import com.plexure.di.ActivityModule
import com.plexure.extentions.toast
import com.plexure.myapplication.databinding.ActivityMainBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var dataRepository: DataRepository
    @Inject
    lateinit var userStore: UserStore




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)

        setContentView(R.layout.activity_main)
        setupActivityComponent()
    }

    private fun setupActivityComponent() {
        PlexureApplication.get().appComponent.plus(ActivityModule(this)).inject(this)
    }


    fun button1Clicked(@Suppress("UNUSED_PARAMETER") view: View) {
        Log.d("a", "button1Clicked")
        dataRepository
                .getApiData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {
                    test_view.text = it.toString()
                    toast("http request done")
                }
                .subscribe ()


    }

    fun button2Clicked(@Suppress("UNUSED_PARAMETER") view: View) {
        dataRepository
                .getUserData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess {

                    Log.d("a", "button1Clicked$it")
                    var str = ""
                    it.forEach {
                        user ->
                        str += "\n\t $user" }
                    test_view.text = str
                    toast("db request done")
                }
                .subscribe ()
    }


}
