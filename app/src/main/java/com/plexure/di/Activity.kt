package com.plexure.di

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.plexure.myapplication.MainActivity
import dagger.Module
import dagger.Provides
import dagger.Subcomponent
import javax.inject.Scope

/**
 * @author fangzhzh
 * @since Dec 13, 18 9:52 PM
 */

@Scope
annotation class ActivityScope


@ActivityScope
@Subcomponent(modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    abstract fun inject(activity: MainActivity)

}

@Module
class ActivityModule(private val activity: AppCompatActivity) {
    @ActivityScope
    @Provides
    internal fun provideContext(): Context {
        return activity
    }


}