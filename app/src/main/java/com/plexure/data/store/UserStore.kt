package com.plexure.data.store

import com.plexure.database.DBUser
import com.plexure.database.PlexureDatabase
import javax.inject.Inject


class UserStore @Inject
constructor(private val databaseManager: PlexureDatabase) {
    fun getAllUsers(): List<User> = databaseManager.userDao().all.map {
        dbUser ->
        User(
                id = dbUser.id,
                name = dbUser.name
        )
    }
    fun saveUsers(userList: List<User>) {
        databaseManager.userDao().insertAll(mapOrgUserList2DB(userList))
    }

    private fun mapOrgUserList2DB(userList: List<User>): List<DBUser> {
        return userList.map {
            DBUser(
                    id = it.id,
                    name = it.name
            )
        }
    }

    fun removeAll() {
        databaseManager.userDao().removeAll()
    }

    fun mockData() {
        saveUsers(listOf(
                User(id="1",name = "Benjamin"),
                User(id="2",name = "Margaret"),
                User(id="3",name = "Gordon"),
                User(id="4",name = "Eric"),
                User(id="4",name = "Jordan")
        ))
    }
}


data class User(var id: String = "", val name: String = "")