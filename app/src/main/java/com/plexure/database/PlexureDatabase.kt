package com.plexure.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
        entities = [
            DBUser::class],
        version = 1,
        exportSchema = false)
abstract class PlexureDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}