package com.plexure.extentions

import android.content.Context
import android.widget.Toast

/**
 * @author fangzhzh
 * @since Dec 13, 18 10:51 PM
 */

fun Context.toast(message: CharSequence) =
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()