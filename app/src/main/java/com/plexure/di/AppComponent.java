package com.plexure.di;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author fangzhzh
 * @since Dec 13, 18 9:46 PM
 */
@Singleton
@Component(
        modules = {
                AppModule.class,
        }
)
public interface AppComponent {
        ActivityComponent plus(ActivityModule activityModule);
}
