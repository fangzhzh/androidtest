package com.plexure.api

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * @author fangzhzh
 * @since Dec 13, 18 10:06 PM
 */


class Args {
    override fun toString(): String {
        return "Args"
    }
}

data class BinResponse (

    @SerializedName("args")
    @Expose
    var args: Args? = null,

    @SerializedName("data")
    @Expose
    var data: String? = null,

    @SerializedName("files")
    @Expose
    var files: Files? = null,

    @SerializedName("form")
    @Expose
    var form: Form? = null,

    @SerializedName("headers")
    @Expose
    var headers: Headers? = null,

    @SerializedName("json")
    @Expose
    var json: Any? = null,

    @SerializedName("origin")
    @Expose
    var origin: String? = null,

    @SerializedName("url")
    @Expose
    var url: String? = null


) {
    override fun toString(): String {
        return "BinResponse(" +
                "\n  args=$args, " +
                "\n  data=$data, " +
                "\n  files=$files, " +
                "\n  form=$form, " +
                "\n  headers=$headers, " +
                "\n  json=$json, " +
                "\n  origin=$origin, " +
                "\n  url=$url" +
                ")"
    }
}


class Files {
    override fun toString(): String {
        return "Files"
    }
}


class Form {
    override fun toString(): String {
        return "Form"
    }


}

class Headers {
    @SerializedName("Accept")
    @Expose
    var accept: String? = null
    @SerializedName("Accept-Encoding")
    @Expose
    var acceptEncoding: String? = null
    @SerializedName("Cache-Control")
    @Expose
    var cacheControl: String? = null
    @SerializedName("Connection")
    @Expose
    var connection: String? = null
    @SerializedName("Content-Length")
    @Expose
    var contentLength: String? = null
    @SerializedName("Content-Type")
    @Expose
    var contentType: String? = null
    @SerializedName("Host")
    @Expose
    var host: String? = null
    @SerializedName("Postman-Token")
    @Expose
    var postmanToken: String? = null
    @SerializedName("User-Agent")
    @Expose
    var userAgent: String? = null

    override fun toString(): String {
        return "Headers(" +
                "\n\t\taccept=$accept, " +
                "\n\t\tacceptEncoding=$acceptEncoding, " +
                "\n\t\tcacheControl=$cacheControl, " +
                "\n\t\tconnection=$connection, " +
                "\n\t\tcontentLength=$contentLength, " +
                "\n\t\tcontentType=$contentType, " +
                "\n\t\thost=$host, " +
                "\n\t\tpostmanToken=$postmanToken, " +
                "\n\t\tuserAgent=$userAgent)"
    }

}